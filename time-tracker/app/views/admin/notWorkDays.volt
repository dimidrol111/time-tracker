{{ content() }}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="container">
    <div class="row">
        <div class="col">
            <h2>Non-working days</h2>
        <div class="col mt-5">
          <h4>Adding a non-working day</h4>
          <p><label for="fname">Date:</label>
          <input type="date" id="fname" name="fname" data-date-format="YYYY MM DD " value="2020-01-01"></p>
          <p><label for="fname">Repeat next year?</label><input type="checkbox" id="checkNotWork">
          </p>
          <p><button type="button" class="btn" id="addNotWork">Add</button></p>
        </div>
    </div>
</div>
<div class="table-responsive">
    {% for data in notWorkDays %}
        {% if loop.first %}
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Repeat next year?</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                {% endif %}
                <tr>
                    <td>{{ data.date }}</td>
                      <td>
                        {% if data.every_year == 1 %}
                        <p<input type="checkbox" id="every_year" checked="checked" data-id="{{data.id}}"></p>
                        {% else %}
                        <p><input type="checkbox" id="every_year"  data-id="{{data.id}}"></p>
                        {% endif %}
                      </td>
                    <td width="12%"><button type="button" class="btn btn-danger" id="delete_notWork" style="display: show" data-id="{{data.id}}"">Delete</button></td>
                </tr>
                {% if loop.last %}
                </tbody>
            </table>
        {% endif %}
    {% else %}
       No data
    {% endfor %}
</div>
<script>
$(function () {
    $(document).on('click', '#delete_notWork', function () {
        var id = $(this).data('id');
        $.ajax({
            method: "POST",
            url: '/admin/deleteNotWork',
            data: {id:id},
            dataType: "JSON",
        }).done(function (response){
           location.reload(true);
        });
    });
});
$(function () {
    $(document).on('click', '#addNotWork', function () {
        var date = $('#fname').val();
        if($('#checkNotWork').is(":checked")){
            var active = 1;
            }
            else if($('#checkNotWork').is(":not(:checked)")){
            var active = 0;
            }
        $.ajax({
            method: "POST",
            url: '/admin/addNotWork',
            data: {date:date,active:active},
            dataType: "JSON",
        }).done(function (response){
           location.reload(true);
        });
    });
});

$(function () {
    $(document).on('click', '#every_year', function () {
        var id = $(this).data('id');
        if($(this).is(":checked")){
            var id = $(this).data('id');
            var active = 1;
        }
        else if($(this).is(":not(:checked)")){
            var id = $(this).data('id');
            var active = 0;
        }
          $.ajax({
               method: "POST",
               url: '/admin/repeatNotWork',
               data: {id:id,active:active},
               dataType: "JSON",
          }).done(function (response){
             console.log(response);
             location.reload(true);
             });
        });
});
</script>