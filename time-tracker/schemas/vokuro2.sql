-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 12 2020 г., 19:10
-- Версия сервера: 8.0.19
-- Версия PHP: 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `vokuro2`
--

-- --------------------------------------------------------

--
-- Структура таблицы `late`
--

CREATE TABLE `late` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `late`
--

INSERT INTO `late` (`id`, `user_id`, `date`, `time`) VALUES
(791, 4, '2020-09-08', '11:03:14'),
(794, 4, '2020-09-10', '16:49:20'),
(795, 5, '2020-09-03', '11:10:00');

-- --------------------------------------------------------

--
-- Структура таблицы `notworkdays`
--

CREATE TABLE `notworkdays` (
  `id` int NOT NULL,
  `date` date DEFAULT NULL,
  `every_year` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `notworkdays`
--

INSERT INTO `notworkdays` (`id`, `date`, `every_year`) VALUES
(27, '2020-09-29', 0),
(28, '2020-09-24', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` int UNSIGNED NOT NULL,
  `profilesId` int UNSIGNED NOT NULL,
  `resource` varchar(16) NOT NULL,
  `action` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `profilesId`, `resource`, `action`) VALUES
(1, 3, 'users', 'index'),
(2, 3, 'users', 'search'),
(3, 3, 'profiles', 'index'),
(4, 3, 'profiles', 'search'),
(17, 2, 'users', 'index'),
(18, 2, 'users', 'search'),
(19, 2, 'users', 'edit'),
(20, 2, 'users', 'create'),
(21, 2, 'profiles', 'index'),
(22, 2, 'profiles', 'search'),
(102, 1, 'users', 'index'),
(103, 1, 'users', 'search'),
(104, 1, 'users', 'edit'),
(105, 1, 'users', 'create'),
(106, 1, 'users', 'delete'),
(107, 1, 'users', 'changePassword'),
(108, 1, 'profiles', 'index'),
(109, 1, 'profiles', 'search'),
(110, 1, 'profiles', 'edit'),
(111, 1, 'profiles', 'create'),
(112, 1, 'profiles', 'delete'),
(113, 1, 'permissions', 'index'),
(114, 1, 'index', 'index');

-- --------------------------------------------------------

--
-- Структура таблицы `profiles`
--

CREATE TABLE `profiles` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `active` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `profiles`
--

INSERT INTO `profiles` (`id`, `name`, `active`) VALUES
(1, 'Administrators', 'Y'),
(2, 'Users', 'Y'),
(3, 'Read-Only', 'Y');

-- --------------------------------------------------------

--
-- Структура таблицы `starttime`
--

CREATE TABLE `starttime` (
  `id` int NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `starttime`
--

INSERT INTO `starttime` (`id`, `time`) VALUES
(1, '09:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `timesheet`
--

CREATE TABLE `timesheet` (
  `id` int NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `date` date DEFAULT NULL,
  `user_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `timesheet`
--

INSERT INTO `timesheet` (`id`, `start_time`, `end_time`, `date`, `user_id`) VALUES
(141, '06:40:34', '11:25:14', '2020-09-10', 5),
(145, '13:29:03', '16:50:00', '2020-09-10', 5),
(146, '18:01:03', '21:22:03', '2020-09-10', 5),
(147, '17:07:57', '17:07:58', '2020-09-10', 5),
(148, '17:08:01', '17:08:03', '2020-09-10', 5),
(149, '18:08:57', '19:08:58', '2020-09-10', 5),
(157, '06:17:00', '13:07:05', '2020-09-11', 5),
(158, '14:07:08', '14:09:09', '2020-09-11', 5),
(159, '14:13:20', '14:13:21', '2020-09-11', 5),
(160, '14:13:26', '14:13:26', '2020-09-11', 5),
(161, '15:17:32', '15:17:33', '2020-09-11', 5),
(162, '09:52:02', '19:06:02', '2020-09-03', 4),
(163, '16:29:17', '16:29:17', '2020-09-11', 5),
(164, '17:52:29', '17:52:33', '2020-09-11', 5),
(165, '18:01:00', '18:01:03', '2020-09-11', 5),
(166, '18:06:01', '22:02:02', '2020-09-11', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` char(60) NOT NULL,
  `profilesId` int UNSIGNED NOT NULL,
  `active` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `profilesId`, `active`) VALUES
(4, 'Yukimi Nagano', 'yukimi@phalconphp.com', '$2a$08$cxxpy4Jvt6Q3xGKgMWIILuf75RQDSroenvoB7L..GlXoGkVEMoSr.', 2, 'N'),
(5, 'Dimon', 'dimidrol@mail.ru', '$2y$08$OHNwQVc0NWxmZWlKZldrVeSg/7NL7wYcVDF3hVrpSyXilef2KGOfe', 2, 'Y'),
(7, 'admin', 'admin@mail.ru', '$2y$08$UEpKVlBSa3hjangreHZCSusIIyVoilZ/Ha.KyQPlplLJkhK2K9W2m', 1, 'Y'),
(8, 'Test', 'test@mail.ru', '$2y$08$UDhJeHVwQXNkbW5TN1Zxd.EhrBgPGDAY9BPQo1qbHdvtg7Z1ZQ2Ma', 2, 'Y');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `late`
--
ALTER TABLE `late`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `notworkdays`
--
ALTER TABLE `notworkdays`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profilesId` (`profilesId`);

--
-- Индексы таблицы `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `active` (`active`);

--
-- Индексы таблицы `starttime`
--
ALTER TABLE `starttime`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `timesheet`
--
ALTER TABLE `timesheet`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profilesId` (`profilesId`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `late`
--
ALTER TABLE `late`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=796;

--
-- AUTO_INCREMENT для таблицы `notworkdays`
--
ALTER TABLE `notworkdays`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT для таблицы `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `starttime`
--
ALTER TABLE `starttime`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `timesheet`
--
ALTER TABLE `timesheet`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
