<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<h4 class="text-center"></h4>
      <button type="button" class="btn btn-primary" id="show_report">Show</button>
      <button type="button" class="btn btn-danger" id="hide_report">Hide</button>
<p>
{% if role == 'Administrators' %}
<p>{{ link_to('admin/late', 'Late page') }}</p>
<p>{{ link_to('admin/notWorkDays', 'Holidays page') }}</p>
<p>{{ link_to('users/search', 'Users') }}</p>
{% endif %}
</p>
<div class="m-2">
<form>
            <select name="month" class="form-control"style="width: 120px;">
                <?php
                    $month = !empty( $_GET['month'] ) ? $_GET['month'] : 0;
                    for ($i = 0; $i <= 12; ++$i) {
                        $time = strtotime(sprintf('+%d months', $i));
                        $label = date('F ', $time);
                        $value = date('m', $time);

                        $selected = ( $value==$month ) ? ' selected=true' : '';

                        printf('<option value="%s"%s>%s</option>', $value, $selected, $label );
                    }
                ?>
            </select>

            <select name="year" class="form-control" style="width: 120px;margin-top:10px;margin-bottom:10px;">
                <?php

                    $year = !empty( $_GET['year'] ) ? $_GET['year'] : 0;

                    for ($i = 0; $i <= 12; ++$i)  {
                        $time = strtotime(sprintf('-%d years', $i));
                        $value = date('Y', $time);
                        $label = date('Y ', $time);

                        $selected = ( $value==$year ) ? ' selected=true' : '';

                        printf('<option value="%s"%s>%s</option>', $value, $selected, $label);
                    }
                ?>
            </select>
            <input type='submit' />
        </form>
</div>
<div class="row ml-5 mt-5">
    <h3>{{todayMonth}}</h3>
</div>

<div class="ml-5">
    <p>Amount of days in month: {{totalDays}} days</p>
    <p>Amount of working days in month: {{totalWorkingDays }} days</p>
    {%if totalWorkHours < 0%}
    <p>Total work time in this month: 0 hours</p>
    {%else%}
    <p>Total work time in this month: {{totalWorkHours}} hours</p>
    {%endif%}
</div>
 <div>
 <table class="table table-bordered table-striped" align="center">
    <thead>
        <tr class="showOnlyToday">
            <th scope="col">Weekday</th>
            {%for user in users%}
            <th scope="col">{{user.name}}</th>
            {%endfor%}
        </tr>
    </thead>
    <tbody>
        {%for data in data%}
            {% if today == data['number'] %}
                <tr id = "showOnlyToday" class="showOnlyToday">
            {% else %}
                <tr id = "showAll">
            {% endif %}
            {% if data['day']==0 or data['day']==6 or data['NotWorkDay']%}
                <th class="free"><p>{{ data['weekday']}}</p> <p class="text-numb">{{ data['number']}}</p></th>
                {% else %}
                <th><p>{{ data['weekday']}}</p> <p class="text-numb">{{ data['number']}}</p></th>
                {%for start in  data['timeSheet']%}
                    <td>
                {% if role =='Administrators' %}
                {%if start['lunchCheck'] == 0 %}
                 <p><label for="fname">Lunch?</label><input type="checkbox" id="checkLunch" data-date = "{{data['date']}}" data-id ="{{start['id_user']}}"></p>
                 {%else%}
                 <p><label for="fname">Lunch?</label><input type="checkbox" id="checkLunch" checked="checked" data-date = "{{data['date']}}" data-id ="{{start['id_user']}}">
                 {%endif%}
                 </p>
                <p><span><input type="time" value="" id="create_start" data-id="{{start['id_user']}}" data-day ="{{data['number']}}" data-date = "{{data['date']}}"/></span>
                {%for User in  start['trackingByUser']%}
                <p><span><input type="time" value="{{User.start_time}}" id="edit_start" data-id="{{User.id}}"/></span>
                {%if User.forgot == 1%}
                  <p style = "color:red;"><span>FORGOT</span>
                 {%else%}
                <span><input type="time" value="{{User.end_time}}" id="edit_stop" data-id="{{User.id}}"/></span></p>
                 {%endif%}
                 <button type="button" class="btn btn-danger" id="deleteUserTime" style="display: show" data-id="{{User.id}}" data-date = "{{data['date']}}" data-id_user ="{{start['id_user']}}">Delete</button>
                {%endfor%}
                {% if start['totalTimeDay']['hour'] <= 0 and start['totalTimeDay']['minute'] <= 0 %}
                {% else %}
                  <p class="mt-5">Total: {{start['totalTimeDay']['hour']}}:{{start['totalTimeDay']['minute']}} </p>
                 {% endif %}
                {% endif %}
                    {% if role == 'Users'%}
                    {%if  select_year == date('Y') %}
                        {% for User in  start['trackingByUser'] %}
                             <p><span>{{User.start_time}}</span> <span>-</span>
                             {%if User.forgot == 1%}
                              <p style = "color:red;"><span>FORGOT</span>
                             {%else%}
                             <span>{{User.end_time}}</span></p>
                             {%endif%}
                        {%endfor%}
                         {% if start['totalTimeDay']['hour'] <= 0 and start['totalTimeDay']['minute'] <= 0 %}
                         {% else %}
                         <p class="mt-5">Total: {{start['totalTimeDay']['hour']}}:{{start['totalTimeDay']['minute']}} </p>
                         {% endif %}
                    {% endif %}
                    {%endif%}
                         {% if  start['stopButtonStatus'] == 1  and start['day'] == data['number'] and user_id == start['id_user'] and todayMonth == date('F') and select_year == date('Y')  %}
                         <button type="button" class="btn btn-danger" id="stop_button" style="display: show" data-id="{{start['id_stop']}}">Stop</button>
                         {% else %}
                         {% if start['day'] == data['number'] and user_id == start['id_user'] and todayMonth == date('F') and select_year == date('Y') %}
                          <button type="button" class="btn btn-danger" id="start_button" style="display: show" data-id="{{start['id_user']}}">Start</button>
                           {% endif %}
                            {% endif %}
                    </td>
                {%endfor%}
            {% endif %}
            </tr>
        {%endfor%}
    </tbody>
</table>
</div>
<script>
$(function () {
    $(document).on('change', '#select_month', function () {
      var category_id = $("#select_month option:selected").val();
      console.log(category_id);
   });
});

$(function () {
    $(document).on('click', '#show_report', function () {
        $('tr').css('display', 'table-row');
    });
});

$(function () {
    $(document).on('click', '#hide_report', function () {
      $('tr').css('display', 'none');
      $('.showOnlyToday').css('display', 'table-row');

    });
});

$(function () {
    $(document).on('click', '#start_button', function () {
        var id = $(this).data('id');
        $('#stop_button').css('display', 'block');
        $('#start_button').css('display', 'none');
        $.ajax({
                 method: "POST",
                 url: 'index/start',
                 data: {id:id},
                 dataType: "JSON",
               }).done(function (message){
               location.reload(true);;
               });
    });
});
$(function () {
    $(document).on('click', '#stop_button', function () {
        var id = $(this).data('id');
        $('#stop_button').css('display', 'none');
        $('#start_button').css('display', 'block');
        $.ajax({
            method: "POST",
            url: 'index/stop',
            data: {id:id},
            dataType: "JSON",
        }).done(function (message){
            location.reload(true);
        });
    });
});
$(function () {
    $(document).on('change', '#create_start', function () {
        var id = $(this).data('id');
        var date = $(this).data('date')
        var time = $(this).val();
        $.ajax({
            method: "POST",
            url: '/admin/createStart',
            data: {id:id,time:time,date:date},
            dataType: "JSON",
        }).done(function (response){
            console.log(response);
            location.reload(true);
        });
    });
});
$(function () {
    $(document).on('change', '#edit_start', function () {
        var id = $(this).data('id');
        var startTime = $(this).val();
        $.ajax({
            method: "POST",
            url: '/admin/updateUserTime',
            data: {id:id,startTime:startTime},
            dataType: "JSON",
        }).done(function (response){
            console.log(response);

        });
    });
});
$(function () {
    $(document).on('change', '#edit_stop', function () {
        var id = $(this).data('id');
        var endTime = $(this).val();
        $.ajax({
            method: "POST",
            url: '/admin/updateUserTime',
            data: {id:id,endTime:endTime},
            dataType: "JSON",
        }).done(function (response){
            console.log(response);
            location.reload(true);
        });
    });
});
$(function () {
    $(document).on('click', '#checkLunch', function () {
        var id = $(this).data('id');
        var date = $(this).data('date')
        if($(this).is(":checked")){
            var id = $(this).data('id');
            var date = $(this).data('date');
            var active = 1;
        }
        else if($(this).is(":not(:checked)")){
            var id = $(this).data('id');
            var date = $(this).data('date');
            var active = 0;
        }
          $.ajax({
               method: "POST",
               url: '/admin/updateLunch',
               data: {id:id,date:date,active:active},
               dataType: "JSON",
          }).done(function (response){
             console.log(response);
             location.reload(true);
             });
        });
});
$(function () {
    $(document).on('click', '#deleteUserTime', function () {
        var id = $(this).data('id');
        var date = $(this).data('date');
        var id_user = $(this).data('id_user');
        $.ajax({
            method: "POST",
            url: '/admin/deleteUserTime',
            data: {id:id,date:date,id_user:id_user},
            dataType: "JSON",
        }).done(function (response){
           location.reload(true);
        });
    });
});

</script>

<style type="text/css">
.free{
    color: red;
}

tr {
    display : none;
}
.showOnlyToday {
    display: table-row;
}