<?php


namespace Vokuro\Models;

class TimeSheet extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $start_time;

    /**
     *
     * @var string
     */
    public $end_time;

    /**
     *
     * @var string
     */
    public $date;

    /**
     *
     * @var integer
     */
    public $forgot;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("vokuro2");
        $this->setSource("TimeSheet");
    }

    public static function startButton($time,$date,$id)
    {
        $trackingData = new TimeSheet();
        $data = [
            'start_time' => $time,
            'date' => $date,
            'user_id' => $id,

        ];
        $trackingData->save($data);

        $luch = Lunch::findFirst([
            "date = :date: AND user_id = :user_id:",
            'bind' => ['date' => $date, 'user_id' => $id]
        ]);
        if(!$luch){
            Lunch::createLunch($id, $date);
        }
        $startTime = StartTime::findFirst();
        $lateStatus = Late::findFirst([
            "user_id = :user_id: and date = :date:",
            'bind' =>['user_id'=>$id, 'date' => $date]
        ]);
        if(!$lateStatus){
            if($time > $startTime->time){
                $late = new Late();
                $late_data = [
                    'user_id' => $id,
                    'date' => $date,
                    'time' => $time,
                ];
                $late->save($late_data);
            }
        }
        exit(json_encode($trackingData->start_time));
    }

    public static function stopButton($request)
    {
        date_default_timezone_set('Asia/Bishkek');
        $trackingData = TimeSheet::findFirst([
            "id = :id:",
            'bind' => ['id' => $request->getPost('id')]
        ]);
        $data = [
            'end_time' => date('H:i:s', time()),
        ];
        $trackingData->update($data);
        exit(json_encode($trackingData->end_time));
    }

    public static function deleteUserTime($request)
    {
        $trackingData = self::findFirst([
            "id = :id:",
            'bind' => ['id' => $request->getPost('id')]
        ]);
        if ($trackingData != null) {
            $trackingData->delete();
        }
    }

    public static function updateTime($request)
    {
        $timeData = TimeSheet::findFirst([
            "id = :id:",
            'bind' => ['id' => $request->getPost('id')]
        ]);
        if($request->getPost('startTime')){
            $data = [
                'start_time' => $request->getPost('startTime')
            ];
        }else{
            $data = [
                'end_time' => $request->getPost('endTime')
            ];
        }

        $timeData->update($data);

        exit(json_encode($timeData));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'TimeSheet';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TimeSheet[]|TimeSheet|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TimeSheet|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
