<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<h4 class="text-center">    </h4>

<div class="m-2">
<form>
            <select name="month" class="form-control"style="width: 120px;">
                <?php
                    $month = !empty( $_GET['month'] ) ? $_GET['month'] : 0;
                    for ($i = 0; $i <= 12; ++$i) {
                        $time = strtotime(sprintf('+%d months', $i));
                        $label = date('F ', $time);
                        $value = date('m', $time);

                        $selected = ( $value==$month ) ? ' selected=true' : '';

                        printf('<option value="%s"%s>%s</option>', $value, $selected, $label );
                    }
              ?>
            </select>

            <select name="year" class="form-control" style="width: 120px;margin-top:10px;margin-bottom:10px;">
                <?php

                    $year = !empty( $_GET['year'] ) ? $_GET['year'] : 0;

                    for ($i = 0; $i <= 12; ++$i)  {
                        $time = strtotime(sprintf('-%d years', $i));
                        $value = date('Y', $time);
                        $label = date('Y ', $time);

                        $selected = ( $value==$year ) ? ' selected=true' : '';

                        printf('<option value="%s"%s>%s</option>', $value, $selected, $label);
                    }
                ?>
            </select>
            <input type='submit' />
        </form>
</div>
<div class="row ml-5 mt-5">
    <h3>{{todayMonth}}</h3>
</div>
<div class="col-12 mt-5">
<h5> The begining of the work day </h5>
            <span><input type="time" value="{{startTime}}" id="edit_startTime" data-id="{{startId}}"/></span></p>
        </div>
   <div id="myDIV">
 <table class="table table-bordered table-striped" align="center">
    <thead>
        <tr class="showOnlyToday">
            <th scope="col">Weekday</th>
            {%for user in users%}
            <th scope="col">{{user.name}}</th>
            {%endfor%}
        </tr>
    </thead>
    <tbody>
        {%for data in data%}
            {% if today == data['number'] %}
                <tr class="showOnlyToday">
            {% else %}
                <tr>
            {% endif %}
            {% if data['day']==0 or data['day']==6 or data['NotWorkDay']%}
                <th class="free" color =red><p>{{ data['weekday']}}</p> <p class="text-numb">{{ data['number']}}</p></th>
                {% else %}
                <th><p>{{ data['weekday']}}</p> <p class="text-numb">{{ data['number']}}</p></th>

                {%for start in  data['timeSheet']%}
                    <td>
                {%for User in  start['lateUser']%}
                 {{User.time}} <button type="button" class="btn btn-danger" id="delete_late" style="display: show" data-id="{{User.id}}"">Delete</button>
                {%endfor%}
                    </td>
                {%endfor%}
            {% endif %}
            </tr>
        {%endfor%}
    </tbody>
</table>
</div>
<script>
$(function () {
    $(document).on('click', '#delete_late', function () {
        var id = $(this).data('id');
        $.ajax({
            method: "POST",
            url: '/admin/deleteLate',
            data: {id:id},
            dataType: "JSON",
        }).done(function (response){
            console.log(response);
            location.reload(true);
        });
    });
});
$(function () {
    $(document).on('change', '#edit_startTime', function () {
        var id = $(this).data('id');
        var time = $(this).val();
        $.ajax({
            method: "POST",
            url: '/admin/StartWorkingDay',
            data: {id:id,time:time},
            dataType: "JSON",
        }).done(function (response){
            console.log(response);
            location.reload(true);
        });
    });
});
</script>
<style type="text/css">
.free{
    color: red;
}
