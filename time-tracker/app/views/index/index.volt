
{{ content() }}

<div class="container">
    <div class="page-header">
        <h1>Time tracking tool</h1>
    </div>

    <div id="report">{{partial('partials/timeTracker')}}</div>

</div>
