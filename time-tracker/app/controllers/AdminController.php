<?php

namespace Vokuro\Controllers;


use Phalcon\Http\Request;
use Vokuro\Models\Lunch;
use Vokuro\Models\StartTime;
use Vokuro\Models\TimeSheet;
use Vokuro\Models\Users;
use Vokuro\Models\Late;
use Vokuro\Models\NotWorkDays;


class AdminController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
        $this->view->setTemplateBefore('public');
    }

    public function lateAction()
    {

        $identity = $this->session->get('auth-identity');
        $id = $identity['id'];
        $request = new Request();
        $month = $request->get('month') ?? date("m");
        $year = $request->get('year') ?? date("Y");
        date_default_timezone_set('Asia/Bishkek');
        $todayMonth = date("F",mktime(0,0,0, $month,1, $year));
        $users = Users::checkUserProfile($identity);
        $monthData = Time::getData($month, $users, $year, $id);
        $startTime = StartTime::findFirst();


        $this->view->setVars([
            'startTime'=>$startTime->time,
            'startId'=>$startTime->id,
            'select_year' =>$year,
            'user_id' => $id,
            'users' => $users,
            'data' => $monthData['month'],
            'today' => date("d"),
            'todayMonth' => $todayMonth,
            'numTodayMonth' => date("m"),
            'numSelectMonth' => $month,
        ]);
    }

    public function deleteLateAction()
    {
        Late::deleteLate($this->request);
    }

    public function notWorkDaysAction()
    {
        $notWorkData = NotWorkDays::find();
        $this->view->setVars([
            'notWorkDays' => $notWorkData
        ]);
    }

    public function deleteNotWorkAction()
    {
        NotWorkDays::deleteNotWorkDay($this->request);
    }


    public function repeatNotWorkAction()
    {
        NotWorkDays::updateStatusEveryYear($this->request);
    }

    public function addNotWorkAction()
    {
        NotWorkDays::addNotWork($this->request);
    }

    public function createStartAction()
    {
        $this->view->disable();
        date_default_timezone_set('Asia/Bishkek');
        $time = $this->request->getPost('time');
        $date = $this->request->getPost('date');
        $id = $this->request->getPost('id');
        TimeSheet::startButton($time,$date,$id);
    }

    public function deleteUserTimeAction()
    {
        TimeSheet::deleteUserTime($this->request);
        Lunch::deleteLunch($this->request);
    }

    public function updateLunchAction(){
        $id = $this->request->getPost('id');
        $status = $this->request->getPost('active');
        $date = $this->request->getPost('date');
        Lunch::updatelunchStatus($id, $status, $date);
    }

    public function updateUserTimeAction()
    {
        TimeSheet::updateTime($this->request);
    }

    public function StartWorkingDayAction()
    {
        $id = $this->request->getPost('id');
        $time = $this->request->getPost('time');
        StartTime::setStartWorkingDay($id, $time);
    }
}