<?php

namespace Vokuro\Controllers;

use Phalcon\Http\Request;
use Vokuro\Models\Late;
use Vokuro\Models\Lunch;
use Vokuro\Models\StartTime;
use Vokuro\Models\Users;
use Vokuro\Models\TimeSheet;
use Phalcon\Mvc\Model\Query;



/**
 * Display the default index page.
 * Vokuro\Controllers\IndexController
 * @package Vokuro\Controllers
 */
class IndexController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
        $this->view->setTemplateBefore('public');

    }

    public function indexAction ()
    {
        $identity = $this->session->get('auth-identity');
        $id = $identity['id'];
        $request = new Request();
        $month = $request->get('month') ?? date("m");
        $year = $request->get('year') ?? date("Y");
        date_default_timezone_set('Asia/Bishkek');
        $todayMonth = date("F",mktime(0,0,0, $month,1, $year));
        $users = Users::checkUserProfile($identity);
        $monthData = Time::getData($month, $users, $year,$id);
        $totalWorkingDays = Time::getWorkingDays($month, $year);

        $this->view->setVars([
            'select_year' =>$year,
            'user_id' => $id,
            'users' => $users,
            'totalWorkingDays' => $totalWorkingDays - $monthData['notWorkDayCount'],
            'data' => $monthData['month'],
            'totalDays' => $monthData['totalDays'],
            'today' => date("d"),
            'todayMonth' => $todayMonth,
            'numTodayMonth' => date("m"),
            'numSelectMonth' => $month,
            'role' => $identity['profile'],
            'totalWorkHours' => $monthData['TotalWorkHours'],
        ]);
    }

    public function stopAction()
    {
        $this->view->disable();
        TimeSheet::stopButton($this->request);
    }

    public function startAction()
    {
        $this->view->disable();
        $identity = $this->session->get('auth-identity');
        $id = $identity['id'];
        date_default_timezone_set('Asia/Bishkek');
        $time = date('H:i:s', time());
        $date = date('Y:m:d', time());
        TimeSheet::startButton($time, $date, $id);
    }
}
