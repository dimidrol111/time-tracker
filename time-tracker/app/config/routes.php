<?php
/*
 * Define custom routes. File gets included in the router service definition.
 */
$router = new Phalcon\Mvc\Router();
$router->add(
    '/index/index',
    [
        'controller' => 'index',
        'action'     => 'index',
    ]
);
$router->add(
    '/users/index',
    [
        'controller' => 'users',
        'action'     => 'index',
    ]
);
$router->add(
    '/session/login',
    [
        'controller' => 'session',
        'action'     => 'login',
    ]
);

return $router;
