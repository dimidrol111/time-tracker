<?php


namespace Vokuro\Models;

class Lunch extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $date;

    /**
     *
     * @var integer
     */
    public $lunch_status;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("vokuro2");
        $this->setSource("lunch");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'lunch';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Lunch[]|Lunch|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Lunch|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function createLunch($id, $date)
    {
        $lunchData = new Lunch();
        $data = [
            'user_id' => $id,
            'date' => $date

        ];
        $lunchData->save($data);
    }

    public static function deleteLunch($request)
    {
        $date =$request->getPost('date');
        $id = $request->getPost('id_user');
        $lunchData = Lunch::findFirst([
            "user_id = :user_id: AND date = :date:",
            'bind' => ['user_id' => $id, 'date' => $date]
        ]);
        $lunchData->delete();
    }

    public static function updatelunchStatus($id, $status, $date)
    {
        $lunchData = Lunch::findFirst([
            "user_id = :user_id: AND date = :date:",
            'bind' => ['user_id' => $id, 'date' => $date]
        ]);
        if($lunchData)
        {
            $lunchData->lunch_status = $status;
            $lunchData->save();
            exit(json_encode($lunchData));
        }else{
            self::createLunch($id, $date);
        }

    }
    public static function checkLunchStatus($id, $date)
    {
        $lunch = Lunch::findFirst([
            "date = :date: AND user_id = :user_id: And lunch_status = 1",
            'bind' => ['date' => $date, 'user_id' => $id]
        ]);
        if ($lunch) {
            $lunchStatus = $lunch->lunch_status;
        } else {
            $lunchStatus = 0;
        }
        return $lunchStatus;
    }

}
