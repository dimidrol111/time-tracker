<?php

namespace Vokuro\Models;

class NotWorkDays extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $date;

    /**
     *
     * @var string
     */
    public $month;

    /**
     *
     * @var integer
     */
    public $day;

    /**
     *
     * @var integer
     */
    public $every_year;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("vokuro2");
        $this->setSource("notWorkDays");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'notWorkDays';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return NotWorkDays[]|NotWorkDays|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return NotWorkDays|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function deleteNotWorkDay($request)
    {
        $notWorkData = NotWorkDays::findFirst([
            "id = :id:",
            'bind' => ['id' => $request->getPost('id')]
        ]);
        if ($notWorkData != null) {
            $notWorkData->delete();
        }
        exit(json_encode($notWorkData));
    }

    public static function addNotWork($request)
    {
        $notWorkData = new NotWorkDays();
        $date = strtotime($request->getPost('date'));
        $month = date('m', $date);
        $day = date('j', $date);

        $data = [
            'date' => $request->getPost('date'),
            'month'=>$month,
            'day' =>$day,
            'every_year'=>$request->getPost('active')

        ];

        $notWorkData->save($data);
        exit(json_encode($notWorkData));
    }

    public static function updateStatusEveryYear($request)
    {
        $notWorkData = NotWorkDays::findFirst([
            "id = :id:",
            'bind' => ['id' => $request->getPost('id')]
        ]);
        $notWorkData->every_year = $request->getPost('active');
        $notWorkData->save();
        exit(json_encode($notWorkData));
    }
}
