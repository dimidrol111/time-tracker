<?php
namespace Vokuro\Controllers;
use Vokuro\Models\Late;
use Vokuro\Models\Lunch;
use Vokuro\Models\NotWorkDays;
use Vokuro\Models\TimeSheet;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Manager;

class Time extends ControllerBase
{
    public static function getData($selected_month, $users,$year,$id) {
        $month = [];
        $user_id = $id;
        $totalCountHour = 0;
        $number = cal_days_in_month(CAL_GREGORIAN, $selected_month, $year);
        $notWorkDayCount = 0;
        for ($i = 1; $i <= $number; $i++) {
            $dataByUser = [];
            $time = strtotime($year.'/'.$selected_month.'/'.$i);
            $date = date("Y-m-d", $time);
            $day = date("w", $time);
            $weekday =  date("l", $time);
            $notWorkDays = NotWorkDays::findFirst([
                "date = :day:",
                'bind' => ['day' => date("Y:m:d", $time)]
            ]);
            $notWorkEveryYear = NotWorkDays::findFirst([
                "month = :month: AND day = :day: AND every_year = 1",
                'bind' => ['month' => $selected_month,'day'=>date("j", $time)]
            ]);
            if ($notWorkEveryYear or $notWorkDays)
            {
                $notWorkDays = $i;
                $notWorkDayCount++;
            }
            foreach ($users as $user)
            {
                $tracking = TimeSheet::find([
                    "date = :date: AND user_id = :user_id:",
                    'bind' => ['date' => date("Y-m-d", $time), 'user_id' => $user->id]
                ]);
                $lunchCheck = Lunch::checkLunchStatus($user->id, $date);
                $lateDb = Late::find([
                    "user_id = :user: AND date = :date:",
                    'bind' => ['user' => $user->id, 'date' =>$date ]
                ]);
                $stop = 0;
                $id=0;
                foreach ($tracking as $arr){
                    if($arr->end_time == null and $arr->date != date("Y-m-d")){
                        $arr->forgot = '1';
                        $arr->save();
                    }
                    if($arr->end_time == null){
                        $stop = 1;
                        $id = $arr->id;
                    }
                }
                $totalTimeDay = Time::totalTime($tracking);
                if($user->id == $user_id){
                    $totalCountHour += $totalTimeDay['hour'];
                }
                if($lunchCheck){
                    $totalCountHour -=1;
                }
                array_push($dataByUser, [
                    'stopButtonStatus'=>$stop,
                    'lunchCheck'=>$lunchCheck,
                    'id_stop'=>$id,
                    'trackingByUser'=>$tracking,
                    'lateUser'=>$lateDb,
                    'totalTimeDay'=>$totalTimeDay,
                    'id_user'=>$user->id,
                    'day'=>date("d"),
                ]);

            }
            array_push($month, [
                'weekday'=>$weekday,
                'number'=> $i,
                'NotWorkDay' =>$notWorkDays,
                'day'=>$day,
                'date'=>$date,
                'timeSheet'=>$dataByUser]);
        }


        return ['month' => $month,
            'userTotalTime'=>$users,
            'totalDays' => $number,
            'notWorkDayCount'=> $notWorkDayCount,
            'TotalWorkHours' => $totalCountHour,

        ];
    }

    public static function totalTime($time)
    {
        $time_diff = 0;
        $total = 0;
        date_default_timezone_set('Asia/Bishkek');
        foreach ($time as $data) {
            if($data->end_time != null){
                $time_diff = strtotime($data->end_time) - strtotime($data->start_time);
            }
            $total += $time_diff;
        }
        $hour = floor($total/60/60);
        $minute = floor($total/60-$hour*60);

        return ['hour' => $hour, 'minute' => $minute];
    }

    public static function getWorkingDays ($selected_month, $year,$ignore = array(0, 6))
    {
        $count = 0;

        $counter = mktime(0, 0, 0, $selected_month, 1, $year);
        while (date("n", $counter) == $selected_month) {
            if (in_array(date("w", $counter), $ignore) == false) {
                $count++;
            }
            $counter = strtotime("+1 day", $counter);
        }
        return $count ;
    }
}