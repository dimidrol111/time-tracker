<?php

/*
  +------------------------------------------------------------------------+
  | Vökuró                                                                 |
  +------------------------------------------------------------------------+
  | Copyright (c) 2016-present Phalcon Team (https://www.phalconphp.com)   |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconphp.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Vokuro\Forms;

use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Form;
use Vokuro\Models\Profiles;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Email as EmailText;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;
use Vokuro\Models\Users;

/**
 * Vokuro\Forms\UsersForm
 * @package Vokuro\Forms
 */
class CreateUserTimeForm extends Form
{

    public function initialize($entity = null, $options = null)
    {

        // In edition the id is hidden
        if (isset($options['edit']) && $options['edit']) {
            $id = new Hidden('id');
        } else {
            $id = new Text('id');
        }
        $this->add($id);

        $users = Users::find();

        $UsersId = new Select('usersId', $users, [
            'using' => [
                'id',
                'name'
            ],
            'useEmpty' => true,
            'emptyText' => '...',
            'emptyValue' => '',
        ]);
        $UsersId->setLabel('Пользователь');

        $this->add($UsersId);

        $name = new Date('Date', [
            // 'placeholder' => 'Name'
        ]);
        $name->setLabel('Date');
        $name->addValidators([
            new PresenceOf([
                'message' => 'The date is required'
            ])
        ]);
        $this->add($name);

        $name = new DateTime('Time', [
            // 'placeholder' => 'Name'
        ]);
        $name->setLabel('Time');
        $name->addValidators([
            new PresenceOf([
                'message' => 'The Time is required'
            ])
        ]);
        $this->add($name);
    }
}
