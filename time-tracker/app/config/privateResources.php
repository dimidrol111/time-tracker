<?php

use Phalcon\Config;
use Phalcon\Logger;

return new Config([
    'privateResources' => [
        'users' => [
            'index',
            'search',
            'edit',
            'create',
            'delete',
            'changePassword'
        ],
        'profiles' => [
            'index',
            'search',
            'edit',
            'create',
            'delete'
        ],
        'permissions' => [
            'index'
        ],
        'index' => [
            'index',
            'start',
            'stop',
            'updateLunch'
        ],
        'admin'=>[
            'late',
            'notWorkDays',
            'deleteNotWork',
            'repeatNotWork',
            'deleteLate',
            'addNotWork',
            'createStart',
            'updateStartTime',
            'updateStopTime',
            'updateUserTime',
            'deleteUserTime',
            'updateLunch',
            'StartWorkingDay'
        ]
    ]
]);
